<?php
/*
Plugin Name: LandBot
Description: LandBot
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/
	
	add_action( 'edit_form_after_title', function( $post ) 
{
add_thickbox();
?>
<div id="my-content-id" class="bot_container" style="display:none;">
		<div class="cont_main_bot">
		
		<div class="form-group">
			<img src="http://vinylcalc.logicsbuffer.com/wp-content/uploads/2018/06/Modern-Website-Headers-41.jpg">
		</div>
		
		<div class="form-group">  
			<h3>1.Copy and paste your landbot URL here</h3>
			<p><i>*You can find it under your landbot>Shere section</i></p>
		</div>
		
		<div class="form-group">
			<label for="email">URL:</label>
			<input type="email" class="form-control" name="landbot_url" id="landbot_url">
		</div>
		
		<h3>2. Display Format:</h3>
		<p><i>*The way Landbot is displayed</i></p>
		
	
		
		<div class="row main_codetype">
					   
		<div class="col col-sm-3">
			<!--<span class="display_icon_parent" style="background-image: url();" id="lb_picture_1"><input class="lb_input_hide" type="radio" name="landbot_display" id="full_page"></span> -->
			<?php echo '<span id="lb_picture_1" class="display_icon_parent" style="background-image: url('. plugins_url( '/images/icon1.png', __FILE__ ) .')" > '; ?>
			<input class="lb_input_hide" type="radio" name="landbot_display" id="full_page"></span>
			<span class="lb_img_label">Full Page</span>
		</div>
		
		<div class="col col-sm-3">
			<?php echo '<span id="lb_picture_2" class="display_icon_parent" style="background-image: url('. plugins_url( '/images/icon2.png', __FILE__ ) .')" > '; ?><input class="lb_input_hide" type="radio" name="landbot_display" id="popup"></span>
			<span class="lb_img_label">POPUP</span>
		</div>
		
		<div class="col col-sm-3">
		<?php echo '<span id="lb_picture_3" class="display_icon_parent" style="background-image: url('. plugins_url( '/images/icon3.png', __FILE__ ) .')" > '; ?><input class="lb_input_hide" type="radio" name="landbot_display" id="embed"> </span>
		<span class="lb_img_label">EMBED</span>
		</div>
		
		<div class="col col-sm-3">
		<?php echo '<span id="lb_picture_4" class="display_icon_parent" style="background-image: url('. plugins_url( '/images/icon4.png', __FILE__ ) .')" > '; ?><input class="lb_input_hide" type="radio" name="landbot_display" id="livechat"> </span>
		<span class="lb_img_label">LIVE CHAT</span>
		</div>
			
		</div>
		
		<h3>3. More Options:</h3>
		
		<div class="more_option">
			<div class="more_op_label">Hide background:</div>
			<input type="checkbox" name="hide_background" id="hide_background" value="true">
		</div>
		
		<div class="more_option">
			<div class="more_op_label">Hide Header:</div>
			<input type="checkbox" name="hide_header" id="hide_header" value="true">
		</div>
		
		<div class="more_option full_input">
			<div class="more_op_label">Widget Height:(pixels)</div>
			<input type="email" name="widget_height" id="widget_height" value="400" class="form-control" >
		</div>
					
		
		<div class="buttons">
			<input type="button" id="set_shortcode" class="button button-primary button-small" value="OK">
			<input type="button" id="cancel" class="button button-small" value="Cancel">
		</div>
		
		</div>
</div>
<a href="#TB_inline?width=400&height=550&inlineId=my-content-id" class="thickbox button btn_landBot">Add LandBot</a>
<script>
jQuery(document).ready(function(){	
jQuery('.btn_landBot').click(function() { 
	jQuery('#TB_overlay').show();
	jQuery('#TB_window').show();
});
jQuery('#set_shortcode').click(function() { 
var landbot_url = jQuery('#landbot_url').val();
var landbot_display = jQuery('#landbot_display:checked').val();

//if(jQuery('#hide_background:checked')){
if (jQuery('input#hide_background').is(':checked')) {
var hide_background = jQuery('#hide_background:checked').val();
}else{
	var hide_background = 'false';
}
if (jQuery('input#hide_header').is(':checked')) {
var hide_header = jQuery('#hide_header:checked').val();
}else{
var hide_header = 'false';
}
var widget_height = jQuery('#widget_height').val();

var short_code_str = '[landbot url="'+landbot_url+'" format="'+landbot_display+'" hide_background="'+hide_background+'" hide_header="'+hide_header+'" widget_height="'+widget_height+'" ]'
tinymce.activeEditor.setContent(tinyMCE.activeEditor.getContent() + short_code_str); 
	jQuery('#TB_overlay').hide();
	jQuery('#TB_window').hide();
});
jQuery('#cancel').click(function() { 
	jQuery('#TB_overlay').hide();
	jQuery('#TB_window').hide();
});
});

</script>
<?php
});

add_action('wp_enqueue_scripts', 'landbot_script_front_css');
add_action('wp_enqueue_scripts', 'landbot_script_front_js');
add_action('admin_enqueue_scripts', 'landbot_script_back_css');
add_action('init', 'wp_astro_init');
function wp_astro_init() {
   add_shortcode('landbot', 'wp_landbot_form');
}
function wp_landbot_form($atts) {	
extract( shortcode_atts( array (
        'url' => 'https://landbot.io/u/H-44681-49EZ8AYEHF1ZN5JA/index.html',
        'format' => 'embed',
        'hide_background' => 'false',
        'hide_header' => 'false',
        'widget_height' => '400',
    ), $atts ) );
	 ob_start();
	 ?>
	<div id="landbot-wordpress" data-url="<?php echo $url; ?>" data-format="<?php echo $format; ?>" datahide_background="<?php echo $hide_background; ?>"
data-hide_header="<?php echo $hide_header; ?>" data-widget_height="<?php echo $widget_height; ?>"
></div>

<?php

return ob_get_clean();
}
function landbot_script_back_css() {
    wp_register_style('admin_landbot_style', plugins_url('css/admin_landbot.css',__FILE__));
	wp_enqueue_style('admin_landbot_style');
}

function landbot_script_front_css() {
		/* CSS */
        wp_register_style('landbot_style', plugins_url('css/landbot.css',__FILE__));
        wp_enqueue_style('landbot_style');
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function landbot_script_back_js() {
	
}



function landbot_script_front_js() {
 	
			   
        wp_register_script('landbot_script', plugins_url('js/landbot.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('landbot_script');

}
