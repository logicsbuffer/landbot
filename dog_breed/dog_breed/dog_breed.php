<?php
/*
Plugin Name: Dog Breed Calculator
Description: Dog Breed Calculator
Version: 1.0.0
Plugin URI: https://www.fiverr.com/wp_right  
Author: LogicsBuffer
Author URI: http://logicsbuffer.com/
*/

add_action('wp_enqueue_scripts', 'dog_breed_script_front_css');
add_action('wp_enqueue_scripts', 'dog_breed_script_front_js');
add_action('admin_enqueue_scripts', 'dog_breed_script_back_css');
add_action('woocommerce_add_order_item_meta','order_meta_handler', 1, 3);
function order_meta_handler($item_id, $values, $cart_item_key) {
	$cart_session = WC()->session->get('cart');
	if($cart_session[$cart_item_key]['country_name'][0]){
	wc_add_order_item_meta($item_id, "Country Name", $cart_session[$cart_item_key]['country_name'][0]);
	}
	if($cart_session[$cart_item_key]['dog_weight'][0]){
	wc_add_order_item_meta($item_id, "Dog weight", $cart_session[$cart_item_key]['dog_weight'][0]);
	}
	if($cart_session[$cart_item_key]['dog_food_type'][0]){
	wc_add_order_item_meta($item_id, "Dog food type", $cart_session[$cart_item_key]['dog_food_type'][0]);
	}
	if($cart_session[$cart_item_key]['neutered'][0]){
	wc_add_order_item_meta($item_id, "Neutered", $cart_session[$cart_item_key]['neutered'][0]);
	}
	if($cart_session[$cart_item_key]['dogbreeds'][0]){
	wc_add_order_item_meta($item_id, "Dog Breed", $cart_session[$cart_item_key]['dogbreeds'][0]);
	}
	if($cart_session[$cart_item_key]['gender'][0]){
	wc_add_order_item_meta($item_id, "Gender", $cart_session[$cart_item_key]['gender'][0]);
	}
	if($cart_session[$cart_item_key]['age'][0]){
	wc_add_order_item_meta($item_id, "Dog Age", $cart_session[$cart_item_key]['age'][0]);
	}
	if($cart_session[$cart_item_key]['how_active'][0]){
	wc_add_order_item_meta($item_id, "Active", $cart_session[$cart_item_key]['how_active'][0]);
	}
	if($cart_session[$cart_item_key]['dog_type'][0]){
	wc_add_order_item_meta($item_id, "Dog Type", $cart_session[$cart_item_key]['dog_type'][0]);
	}
	if($cart_session[$cart_item_key]['country_name'][0]){
	wc_add_order_item_meta($item_id, "Country Name", $cart_session[$cart_item_key]['country_name'][0]);
	}
	
}
add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data11', 10, 2);
function add_cart_item_custom_data11($cart_item_meta, $new_post_id) {
	$dog_weight = get_post_meta( $new_post_id,'dog_weight'  );
	$how_active= get_post_meta( $new_post_id,'how_active');
	$dog_type= get_post_meta( $new_post_id,'dog_type');
	$user_name= get_post_meta( $new_post_id,'user_name');
	$user_email= get_post_meta( $new_post_id,'user_email');
	$dog_food_type= get_post_meta( $new_post_id,'dog_food_type');
	$neutered= get_post_meta( $new_post_id,'neutered');
	$dogbreeds= get_post_meta( $new_post_id,'dogbreeds');
	$gender= get_post_meta( $new_post_id,'gender');
	$age= get_post_meta( $new_post_id,'age');
	$country_name = get_post_meta( $new_post_id,'country_name' );
	$cart_item_meta['dog_weight'] = $dog_weight;
	$cart_item_meta['how_active'] = $how_active;
	$cart_item_meta['neutered'] = $neutered;
	$cart_item_meta['dog_type'] = $dog_type;
	$cart_item_meta['user_name'] = $user_name;
	$cart_item_meta['user_email'] = $user_email;
	$cart_item_meta['dogbreeds'] = $dogbreeds;
	$cart_item_meta['age'] = $age;
	$cart_item_meta['country_name'] = $country_name;
	return $cart_item_meta;
}
/**
 * Register a custom menu page.
 */
function wpdocs_register_my_custom_menu_page(){
    add_menu_page( 
        __( 'Dog Breed Setting', 'textdomain' ),
        'Dog Breed',
        'manage_options',
        'dog_breed',
        'dog_breed_menu_page'
    ); 
}
add_action( 'admin_menu', 'wpdocs_register_my_custom_menu_page' );
 
/**
 * Display a custom menu page
 */
function dog_breed_menu_page(){
	if(isset($_POST['submit_feed_setting'])){
		$kcal_per_food = $_POST['kcal_per_food']; 
		$price_per_kg = $_POST['price_per_kg']; 
		$threepl = $_POST['3PL'];
		$packaging  = $_POST['packaging'];
		$cart_page_disc  = $_POST['cart_page_disc'];

		update_option('kcal_per_food',$kcal_per_food);
		update_option('price_per_kg',$price_per_kg);
		update_option('threepl',$threepl);
		update_option('packaging',$packaging);
		update_option('cart_page_disc',$cart_page_disc);
	}
	$kcal_per_food = get_option('kcal_per_food');
	$price_per_kg = get_option('price_per_kg');
	$threepl = get_option('threepl');
	$packaging = get_option('packaging');
	$cart_page_disc_show = get_option('cart_page_disc');

   ?>
   <form action="" class="ci_form" id="form" method="post">
<h2>Dog Breed Setting</h2>

<div class="">
<label>Kcal per dog food kilogram</label>

<input type="text" class="form-control dog_name_1" placeholder="Kcal per dog food kilogram" value="<?php echo $kcal_per_food ?>" required="required" name="kcal_per_food">
</div><br>

<div class="">
<label>Dog food price per kilo</label>
<input type="text" class="form-control dog_name_1" placeholder="Dog food price per kilo" value="<?php echo $price_per_kg ?>" required="required" name="price_per_kg">
</div><br>
<div class="">
<label>3PL</label>
<input type="text" class="form-control dog_name_1" placeholder="3PL" required="required" value="<?php echo $threepl ?>" name="3PL">
</div><br>
<div class="">
<label>PACKAGING</label>
<input type="text" class="form-control dog_name_1" placeholder="PACKAGING" required="required" value="<?php echo $packaging ?>" name="packaging">
</div><br>
<div class="">
<label>Card page Discount in Percentage(Enter value from 0-100)</label><br/>
<input type="text" class="form-control cart_page_disc" placeholder="Enter value from 0-100"  value="<?php echo $cart_page_disc_show ?>" name="cart_page_disc">
</div><br>
<input type="submit" name="submit_feed_setting" id="feed_setting" value="Submit">
</form>
   <?php
}
add_action('init', 'wp_dog_breed_init');
function wp_dog_breed_init() {
   add_shortcode('show_dog_breed_form', 'wp_dog_breed_form');
}
function wp_dog_breed_form($atts) {
	if(isset($_POST['submit_diet'])){
		$dog_name_1 = $_POST['dog_name_1'];
		$country_name_1  = $_POST['country_name_1'];
		$gender= $_POST['gender'];
		$neutered = $_POST['neutered'];
		$years = $_POST['years'];
		$months = $_POST['months']; 
		$age = $months.' months'.$years.' years';
		$dogbreeds = $_POST['dogbreeds']; 
		$dog_food_type= $_POST['dog_food_type'];
		$dog_weight= $_POST['dog_weight'];
		$dog_type = $_POST['dog_type'];
		$how_active= $_POST['how_active'];
		$user_name= $_POST['user_name'];
		$user_email = $_POST['user_email'];

	if($how_active == 'lazy' && $dog_type == 'skinny'){
		$calorie_per_kg = 38;
	}
	elseif($how_active == 'active' && $dog_type == 'skinny'){
		$calorie_per_kg = 42;
	}elseif($how_active == 'hyper' && $dog_type == 'skinny'){
		$calorie_per_kg = 42;
	}elseif($how_active == 'lazy' && $dog_type == 'just_right'){
		$calorie_per_kg = 33;
	}
	elseif($how_active == 'active' && $dog_type == 'just_right'){
		$calorie_per_kg = 38;
	}elseif($how_active == 'hyper' && $dog_type == 'just_right'){
		$calorie_per_kg = 42;
	}elseif($how_active == 'lazy' && $dog_type == 'cubby'){
		$calorie_per_kg = 33;
	}
	elseif($how_active == 'active' && $dog_type == 'cubby'){
		$calorie_per_kg = 33;
	}elseif($how_active == 'hyper' && $dog_type == 'cubby'){
		$calorie_per_kg = 33;
	}
	
	$calorie_per_day = $calorie_per_kg * $dog_weight;
	?><pre><?php print_r('calorie_per_day: '.$calorie_per_day); ?> </pre><?php

    $calorie_per_month = 	$calorie_per_day * 28;
    ?><pre><?php print_r('calorie_per_month: '.$calorie_per_month); ?> </pre><?php

    $calorie_per_month_k = $calorie_per_month / 1000;
    ?><pre><?php print_r('calorie_per_month_k: '.$calorie_per_month_k); ?> </pre><?php

    $food_calorie_per_kg = get_option('kcal_per_food');
        //$total_g = $calorie_per_month  / $food_calorie_per_kg ;
    $total_kg = $calorie_per_month_k  / $food_calorie_per_kg ;
    ?><pre><?php print_r('food_calorie_per_kg: '.$food_calorie_per_kg); ?> </pre><?php
    ?><pre><?php print_r('total_kg: '.$total_kg); ?> </pre><?php


	//$total_kg = $total_g / 1000;

	//$country_name = get_page_by_title( 'Australia (AU)', , 'countries' );
	$country_obj = get_page_by_title($country_name_1, OBJECT, 'countries');
	$country_id = $country_obj->ID;
	$tax_str = get_field( "tax",$country_id);
	$tax = trim($tax_str,"%");
	?><pre><?php print_r('tax: '.$tax); ?> </pre><?php

	//
	$dog_food_price_per_kg_str = get_option('price_per_kg');
	$dog_food_price_per_kg = floatval($dog_food_price_per_kg_str);
	$threepl = get_option('threepl');
	$packaging = get_option('packaging');
	$shipping = $total_kg + 1;
	?><pre><?php print_r('shipping: '.$shipping); ?> </pre><?php
	 
	 $customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => 'shop_order', // WC orders post type
        'post_status' => 'wc-completed' // Only orders with status "completed"
    ) );
    // Count number of orders
    $count = count( $customer_orders );

    // return "true" when customer has already one order
    if ( $count > 0 ){
		$food_price_only = $total_kg * $dog_food_price_per_kg;
		//$food_price_with_out_tax = $threepl + $packaging + $food_price_only + 23 * 1.15;
		$food_price_with_out_tax = $threepl + $packaging + $food_price_only;
	}else{
		$food_price_only = $total_kg * 0.625 * $dog_food_price_per_kg;
		//$food_price_with_out_tax = $threepl + $packaging + $food_price_only + 23 * 1.15;
		$food_price_with_out_tax = $threepl + $packaging + $food_price_only;
	}
	$food_price_only = round($food_price_only,2);


?><pre><?php print_r('dog_food_price_per_kg: '.$dog_food_price_per_kg); ?> </pre><?php
?><pre><?php print_r('food_price_only: '.$food_price_only); ?> </pre><?php
?><pre><?php print_r('total_kg: '.$total_kg); ?> </pre><?php
?><pre><?php print_r('packaging: '.$packaging); ?> </pre><?php
?><pre><?php print_r('threepl: '.$threepl); ?> </pre><?php

$food_price_percent = $food_price_with_out_tax/100;
$food_price_tax = $food_price_percent * $tax;
$food_price = $food_price_with_out_tax + $food_price_tax;
$food_price = round($food_price,2);
//$food_price = round($food_price_with_out_tax,2);
?><pre><?php print_r('food_price_with_out_tax: '.$food_price_with_out_tax); ?> </pre><?php
?><pre><?php print_r('food_price_with_tax: '.$food_price); ?> </pre><?php

	$new_post = array(
			'post_title' => $dog_name_1,
			'post_status' => 'publish',
			'post_type' => 'product'
			);
			$skuu = "custom-breed";
			$new_post_id = wp_insert_post($new_post);
			update_post_meta($new_post_id, '_sku', $skuu );
			update_post_meta( $new_post_id, '_price', $food_price );
			update_post_meta( $new_post_id,'_regular_price', $food_price );
			update_post_meta( $new_post_id,'_weight', $total_kg );
			update_post_meta( $new_post_id,'country_name', $country_name_1 );
			update_post_meta( $new_post_id,'dog_weight', $dog_weight );
			update_post_meta( $new_post_id,'how_active', $how_active );
			update_post_meta( $new_post_id,'dog_type', $dog_type );
			update_post_meta( $new_post_id,'user_name', $user_name );
			update_post_meta( $new_post_id,'user_email', $user_email );
			update_post_meta( $new_post_id,'dog_food_type', $dog_food_type );
			update_post_meta( $new_post_id,'neutered', $neutered );
			update_post_meta( $new_post_id,'dogbreeds', $dogbreeds );
			update_post_meta( $new_post_id,'gender', $gender );
			update_post_meta( $new_post_id,'age', $age );
			global $woocommerce;
			$woocommerce->cart->add_to_cart($new_post_id);
			$cart_url = site_url().'/cart';				
			?>
			<script type="text/javascript">
			   document.location.href="<?php echo $cart_url; ?>";
		   </script>
			<?php
	 
	}

ob_start();

	?>
				
    
  <link href="css/style.css" rel="stylesheet">

    
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  
 
<script>      
$(document).ready(function(){
    $("#add_dog").click(function(){
        $("#inner_sec").slideToggle("slow");
    });
});
    
</script>
    
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
	<div class="container" style="position:relative; margin-top: 50px;">
		<form method="post" id="formDogs">
            <div id="section1" class="col-md-12" style="">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:0%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>0%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           TELL US<br>ABOUT YOUR DOG
                        </h2>
                    </div>
                </div>
            </div> 
            <div class="row">
                <div class="col-md-3">
                </div>    
                <div class="col-md-6">
                    
                        <div class="form-group" style="margin-bottom: 30px;">
                            <input type="text" class="form-control dog_name_1" placeholder="My dog’s name is..." required="required" name="dog_name_1">
                        </div>
                        <div class="form-group" style="margin-bottom: 30px;">
                        <select class="form-control" id="country_name" type="text" name="country_name_1" required="required">
                            <option>Country where your pup lives in...</option>
                            <?php
                            $args = array(
                                'post_type'        => 'countries',
								'orderby'        => 'name',
								'order'          => 'ASC',
                                'posts_per_page'        => -1
                            );
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) {
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    echo '<option value="'.get_the_title().'" shipping="'. get_post_meta(get_the_ID(), 'Shipping', true ).'" tax="'.get_post_meta(get_the_ID(), 'tax', true ).'">' . get_the_title() . '</option>';

                                } // end while
                            } // end if
                            wp_reset_query();
                            ?>
                        </select>
                        </div>
                        <!--<ul style="list-style: none; margin: 50px 0px 0px 0px; padding: 0px;" class="text-center">-->
                        <!--    <li class="plus_icon" id="add_dog"><span class="plus_icon_img"><img src="img/icon_1.png"></span>ADD ANOTHER DOG</li>-->
                        <!--</ul>-->
                        <!--<div class="inner_section" style="display: none;margin: 50px 0px;" id="inner_sec">-->
                        <!--    <div class="form-group" style="margin-bottom: 30px;">-->
                        <!--    <input type="text" class="form-control" placeholder="My dog’s name is..." name="dog_name_2">-->
                        <!--</div>-->
                        <!--<div class="form-group" style="margin-bottom: 30px;">-->
                        <!--<select class="form-control" type="text" name="country_name_2">-->
                        <!--      <option>Country where your pup lives in...</option>-->
                        <!--      <option value="Afghanistan">Afghanistan</option>-->
                        <!--      <option value="Albania">Albania</option>-->
                        <!--      <option value="Algeria">Algeria</option>-->
                        <!--      <option value="Andorra">Andorra</option>-->
                        <!--      <option value="Angola">Angola</option>-->
                        <!--</select>-->
                        <!--</div>-->
                        
                        <!--</div>-->
                    
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <button type="button" class="btn" id="go-sec-2">NEXT ❯</button>    
                    </div>
                </div>
            </div>
            </div>
            <div id="section2" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:14%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>14%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                            <span class="dogName"></span> IS A...
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-1" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6">
                    <div class="row">

                        <div class="select_dog text-center">
                            <input id="boy" type="radio" name="gender" checked value="boy" required="required" class="gender-cc boy"/>
                            <input id="girl" type="radio" name="gender" value="girl" required="required" class="gender-cc girl"/>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-3" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>
<!--                <div id="boy_section" style="display: none; position: relative;">-->
            <div id="section3" class="col-md-12" style="display: none;" >
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:29%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>29%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           IS <span class="bookie_name"></span>  <span class="is_book"></span> ?
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                 <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-2" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6">
                    <div class="row">
                        <div class="select_dog text-center">
                         <input type="radio" name="neutered" checked value="Yes" required="required" class="neutered-cc neutered_yes"/>
                         <input type="radio" name="neutered" value="No" required="required" class="neutered-cc neutered_no"/>
                        </div>
                    </div>
                        <div class="section-title-header text-center">
                            <h2>
                                AND <span class="set_gender"></span> WAS BORN IN...
                            </h2>
                        </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Year</label>
                               <select name="years" width="100%" class="form-control">
                                <option value="1" width="100%">1 YEAR and ...</option>
                                <option value="2">2 YEARS and ...</option>
                                <option value="3">3 YEARS and ...</option>
                                <option value="4">4 YEARS and ...</option>
                                <option value="5">5 YEARS and ...</option>
                                <option value="6">6 YEARS and ...</option>
                                <option value="7">7 YEARS and ...</option>
                                <option value="8">8 YEARS and ...</option>
                                <option value="9">9 YEARS and ...</option>
                                <option value="10">10 YEARS and ...</option>
                                <option value="11">11 YEARS and ...</option>
                                <option value="12">12 YEARS and ...</option>
                                <option value="13">13 YEARS and ...</option>
                                <option value="14">14 YEARS and ...</option>
                                <option value="15">15 YEARS and ...</option>
                                <option value="16">16 YEARS and ...</option>
                                <option value="17">17 YEARS and ...</option>
                                <option value="18">18 YEARS and ...</option>
                                <option value="19">19 YEARS and ...</option>
                                <option value="20">20 YEARS and ...</option>
                                <option value="21">21 YEARS and ...</option>
                                <option value="22">22 YEARS and ...</option>
                                <option value="23">23 YEARS and ...</option>
                                <option value="24">24 YEARS and ...</option>
                                <option value="25">25 YEARS and ...</option>
                                <option value="26">26 YEARS and ...</option>
                                <option value="27">27 YEARS and ...</option>
                                <option value="28">28 YEARS and ...</option>
                                <option value="29">29 YEARS and ...</option>
                                <option value="30">30 YEARS and ...</option>
                            </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Month</label>
                        <select name="months" width="100%" class="form-control">
                            <option value="1" width="100%">1 MONTH.</option>
                            <option value="2">2 MONTHS.</option>
                            <option value="3">3 MONTHS.</option>
                            <option value="4">4 MONTHS.</option>
                            <option value="5">5 MONTHS.</option>
                            <option value="6">6 MONTHS.</option>
                            <option value="7">7 MONTHS.</option>
                            <option value="8">8 MONTHS.</option>
                            <option value="9">9 MONTHS.</option>
                            <option value="10">10 MONTHS.</option>
                            <option value="11">11 MONTHS.</option>
                        </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-4" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>
            <div id="section4" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:43%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>43%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                            <span class="bookie_new"></span> IS A...
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-3" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 30px;">
                        <select name="dogbreeds" class="form-control" type="text">
						   <option>Your dog’s breed...</option>
                            <?php
                            $args = array(
                                'post_type'        => 'dogs',
								'orderby'        => 'name',
								'order'          => 'ASC',
								'posts_per_page'        => -1
                            );
                            $query = new WP_Query( $args );
                            if ( $query->have_posts() ) {
                                while ( $query->have_posts() ) {
                                    $query->the_post();
                                    echo '<option value="'. get_the_title() . '">' . get_the_title() . '</option>';

                                } // end while
                            } // end if
                            wp_reset_query();
                            ?>
                        </select>
                        </div>
                        <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           AND <span class="eating_value"></span> IS CURRENTLY EATING...
                        </h2>
                        </div>
                        <div class="row">
                            <div class="select_dog text-center">
                                 <input type="radio" name="dog_food_type" checked value="dry food" required="required" class="food-cc food_dry"/>
                                 <input type="radio" name="dog_food_type" value="wet food" required="required" class="food-cc food_wet"/>
                                 <input type="radio" name="dog_food_type" value="home made food" required="required" class="food-cc food_home"/>
                                 <input type="radio" name="dog_food_type" value="rawfood" required="required" class="food-cc food_raw"/>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-5" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>
            <div id="section5" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:57%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>57%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                            <span class="weight_about"></span> WEIGHTS ABOUT
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-4" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6">
                        <div class="form-group" style="margin-bottom: 30px;">
                            <input type="number" name="dog_weight" class="form-control" placeholder="Type how much your dog weights in kilograms (KG)...">
                        </div>
                        <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           <span class="weight_gender"></span> IS...
                        </h2>
                        </div>
                        <div class="row">
                            <div class="select_dog text-center">
                                 <input type="radio" name="dog_type" checked value="skinny" required="required" class="kilograms-cc skinny"/>
                                 <input type="radio" name="dog_type" value="just_right" required="required" class="kilograms-cc justright"/>
                                 <input type="radio" name="dog_type" value="cubby" required="required" class="kilograms-cc cubby"/>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-6" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>
            <div id="section6" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:71%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>71%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                            HOW ACTIVE IS <span class="average"></span>,<br>ON AVERAGE?
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-5" value="❮ BACK">    
                    </div>
                </div>    
                <div class="col-md-6">
                        <div class="row">
                            <div class="select_dog text-center">
                                 <input type="radio" name="how_active" checked value="lazy" required="required" class="actives-cc lazy"/>
                                 <input type="radio" name="how_active" value="active" required="required" class="actives-cc active_img"/>
                                 <input type="radio" name="how_active" value="hyper" required="required" class="actives-cc hyper"/>
                            </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="go-sec-7" value="NEXT ❯">    
                    </div>
                </div>
            </div>
        </div>

            <div id="section7" class="col-md-12" style="display: none;">
            <div class="row">
                <div class="col">
                    <div class="progress progress_costom">
                        <div class="progress-bar" style="width:86%"></div>
                    </div>
                    <div class="section-title text-center">
                        <p>86%</p>
                    </div>
                    <div class="section-title-header text-center" style="margin-top: 40px">
                        <h2>
                           TELL US A BIT<br>ABOUT YOURSELF
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <input type="button" class="btn" id="bk-sec-6" value="❮ BACK">    
                    </div>
                </div>   
                <div class="col-md-6">
                    <form>
                        <div class="form-group" style="margin-bottom: 30px;">
                            <input type="text" name="user_name" class="form-control" placeholder="My name is..." required="required">
                        </div>
                        <div class="form-group" style="margin-bottom: 30px;">
                            <input type="email" name="user_email" class="form-control" placeholder="My email is.." required="required">
                        </div>
                        <div class="row">
                            <div class="col-md-2">
<!--                                <img src="img/button.png" width="100%">-->
                                <input type="checkbox" id="toggle-one" checked data-toggle="toggle" data-size="large" class="btn_togg">
                            </div>
                            <div class="col-md-10 section-title">
                                <p style="line-height: 1;">I’m happy to receive nutritional advice from Rocketo’s inhouse vet, funny dog pictures and occasional promotional offers.<br><span style="color: #76b2a8;"> See Privacy Policy.</span></p>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-3">
                    <div class="form-group text-center" style="margin-top: 65px">
                        <button type="submit" name="submit_diet" class="btn">CALCULATE DIET</button>    

                    </div>
                </div>
            </div>
        </div>
</form>    
              
</div>
            
 <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>     
  <script>
  
$(function() {
    $('#toggle-one').bootstrapToggle();
  })       
var  dog_name_1;      
$(document).ready(function(){
  $("#go-sec-2").click(function(){
     dog_name_1 = $('.dog_name_1').val();
    $("#section2").fadeToggle()
    && $("#section1").fadeOut();
    $('.dogName').html(dog_name_1);  
  });
    
  $("#go-sec-3").click(function(){
      var boy =  $(".boy").prop("checked");
     var girl = $(".girl").prop("checked");
    if(boy == true){
       $('.set_gender').html(dog_name_1); 
       $('.weight_gender').html(dog_name_1); 
       $('.eating_value').html(dog_name_1);
       $('.bookie_name').html(dog_name_1); 
        $('.bookie_new').html(dog_name_1); 
        $('.weight_about').html(dog_name_1);
        $('.average').html(dog_name_1);
       $('.is_book').html('NEUTERED');    
    }
      if(girl == true){
          $('.set_gender').html(dog_name_1);
          $('.weight_gender').html(dog_name_1);
          $('.eating_value').html(dog_name_1);
          $('.bookie_name').html(dog_name_1);
          $('.bookie_new').html(dog_name_1);
          $('.weight_about').html(dog_name_1);
          $('.average').html(dog_name_1);
          $('.is_book').html('SPAYED');
      }
      
    $("#section3").fadeToggle()
 	&& $("#section2").fadeOut();
  });
    
  $("#go-sec-4").click(function(){
    $("#section4").fadeToggle()
 	&& $("#section3").fadeOut();
  });
    
  $("#go-sec-5").click(function(){
    $("#section5").fadeToggle()
 	&& $("#section4").fadeOut();
  });
    
  $("#go-sec-6").click(function(){
    $("#section6").fadeToggle()
 	&& $("#section5").fadeOut();
  });
    
  $("#go-sec-7").click(function(){
    $("#section7").fadeToggle()
 	&& $("#section6").fadeOut();
  });
    
  $("#go-sec-8").click(function(){
    $("#section8").fadeToggle() && $("#section7").fadeOut();
//      $('#formDogs').submit();    
  });
    
  $("#bk-sec-1").click(function(){
    $("#section1").fadeToggle()
 	&& $("#section2").fadeOut();
  });
  
  $("#bk-sec-2").click(function(){
    $("#section2").fadeToggle()
 	&& $("#section3").fadeOut();
  });
    
  $("#bk-sec-3").click(function(){
    $("#section3").fadeToggle()
 	&& $("#section4").fadeOut();
  });
    
  $("#bk-sec-4").click(function(){
    $("#section4").fadeToggle()
 	&& $("#section5").fadeOut();
  });
    
  $("#bk-sec-5").click(function(){
    $("#section5").fadeToggle()
 	&& $("#section6").fadeOut();
  });
    
  $("#bk-sec-6").click(function(){
    $("#section6").fadeToggle()
 	&& $("#section7").fadeOut();
  });
    
  $("#bk-sec-7").click(function(){
    $("#section7").fadeToggle()
 	&& $("#section8").fadeOut();
  });
    
    $("#go-sec-G4").click(function(){
    $("#sectionG4").fadeToggle()
 	&& $("#sectionG3").fadeOut();
  });
    
    $("#go-sec-G5").click(function(){
    $("#sectionG5").fadeToggle()
 	&& $("#sectionG4").fadeOut();
  });
    
     $("#go-sec-G6").click(function(){
    $("#sectionG6").fadeToggle()
 	&& $("#sectionG5").fadeOut();
  });
    
     $("#go-sec-G7").click(function(){
    $("#section7").fadeToggle()
 	&& $("#sectionG6").fadeOut();
  });
    
    $("#bk-sec-G2").click(function(){
    $("#section2").fadeToggle()
 	&& $("#sectionG3").fadeOut();
  });
    
    $("#bk-sec-G3").click(function(){
    $("#sectionG3").fadeToggle()
 	&& $("#sectionG4").fadeOut();
  });
    
    $("#bk-sec-G4").click(function(){
    $("#sectionG4").fadeToggle()
 	&& $("#sectionG5").fadeOut();
  });
    
    $("#bk-sec-G5").click(function(){
    $("#sectionG5").fadeToggle()
 	&& $("#sectionG6").fadeOut();
  });
    
    $("#bk-sec-6").click(function(){
    $("#sectionG6").fadeToggle()
 	&& $("#section7").fadeOut();
  });
    
 $(".boy").click(function(){
      $( ".boy" ).attr( "checked", true );
     $( ".girl" ).attr( "checked", false );
 })
 
 $(".girl").click(function(){
      $( ".boy" ).attr( "checked", false );
      $( ".girl" ).attr( "checked", true );
    
 })
});
    
  </script>
</form>
	<?php
$result = ob_get_clean();
return $result;
	}
function dog_breed_script_back_css() {

}

function dog_breed_script_front_css() {
		/* CSS */
        wp_register_style('dog_breed_style', plugins_url('css/dog_breed.css',__FILE__));
        wp_enqueue_style('dog_breed_style',plugins_url('css/dog_breed.css',__FILE__),10);
}

		add_action( 'wp_ajax_my_ajax_rt', 'my_ajax_rt' );
		add_action( 'wp_ajax_nopriv_my_ajax_rt', 'my_ajax_rt' );
function my_ajax_rt() {
	
}

function dog_breed_script_back_js() {
	
}



function dog_breed_script_front_js() {
 	
			   
        wp_register_script('dog_breed_script', plugins_url('js/dog_breed.js', __FILE__ ),array('jquery'));
        wp_enqueue_script('dog_breed_script');

}
